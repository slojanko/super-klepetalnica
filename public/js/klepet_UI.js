/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
 

 var slika = false;
 var priv = false;
 var pravaImena = [];
 var navideznaImena = [];
 var nalozenaImena = [];

function dodajTabeli(ime, nadimek)
{
    zeObstaja = false;
    for(var i in pravaImena)
    {
        if (pravaImena[i] == ime)
        {
            zeObstaja = true;
            navideznaImena[i] = nadimek;
            break;
        }
    }
    
    if (!zeObstaja)
    {
        pravaImena.push(ime);
        navideznaImena.push(nadimek);
    }
}

 
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo)
{
    // \b               - zacetek besede
    // \S               - karkoli ki ni presledek, tab ali linebreak
    // \.               - dodaj "."
    // ?:jpg|png|gif    - preveri končnico
    // \b               - konec besede
    // TO NE DELA !?!?
    //var re = new RegExp('\\b(http\S+\.+(?:jpg|png|gif))\\b', 'gi');
   // TO PA:
   var re = /\b(http\S+\.+(?:jpg|png|gif))\b/gi;
   
   var temp = vhodnoBesedilo.match(re);
   for(var i in temp)
   {
       if (temp[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") == -1)
       {
            slika = true
            var nov = "<br /><img  width='200' hspace='20' src='" + temp[i] + "' />";
            vhodnoBesedilo += nov;
       }
           
    }
    
    
    return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  slika = false;
  sporocilo = dodajSlike(sporocilo);
  if (jeSmesko || slika == true || priv) {
      priv = false;
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;br /&gt;").join("<br />")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />")
               .split("jpg' /&gt;").join("jpg' />")
               .split("gif' /&gt;").join("gif' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    var jeNajden = false;
    if (sistemskoSporocilo) {
        for(var i in pravaImena)
        {
            if (sistemskoSporocilo.indexOf("(zasebno za " + pravaImena[i] + "): ") == 0 && pravaImena[i] != trenutniVzdevek)
            {
                priv = true;
                sistemskoSporocilo = divElementEnostavniTekst("(zasebno za " + navideznaImena[i] + " (" + pravaImena[i]  + ")): "+sistemskoSporocilo.substr(15 + pravaImena[i].length));
                jeNajden = true;
                break;
            }
        }
        if (!jeNajden)
        {
            for(var j in nalozenaImena)
            {
                if (sistemskoSporocilo.indexOf("(zasebno za " + nalozenaImena[j] + "): ") == 0 && nalozenaImena[j] != trenutniVzdevek)
                {
                    priv = true;
                    sistemskoSporocilo = divElementEnostavniTekst(sistemskoSporocilo);
                    break;
                }
                
            }
            
        }
        $('#sporocila').append(sistemskoSporocilo);
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  // Ko prejme sporocilo, poglej ali je zasebno, to pa tako, da pogledaš ali se začne z imenom uporabnika
  socket.on('sporocilo', function (sporocilo) {

    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    var jeNajden;

    /* ZA TESTIRANJE PRAVILNOSTI
    for(var i in pravaImena)
        console.log(pravaImena.length + " " + pravaImena[i] + " " + navideznaImena[i]);
    */

    for(var i in pravaImena)
    {
        if (((sporocilo.besedilo.indexOf(pravaImena[i]+": ") == 0) || sporocilo.besedilo.indexOf(pravaImena[i]+" (zasebno): ") == 0) && pravaImena[i] != trenutniVzdevek)
        {
            if (sporocilo.besedilo.indexOf(pravaImena[i]+" (zasebno): ") == 0)
                priv = true;
            sporocilo.besedilo = navideznaImena[i] + " (" +  pravaImena[i] + ") " + sporocilo.besedilo.substr(pravaImena[i].length);
            novElement = divElementEnostavniTekst(sporocilo.besedilo);
            jeNajden = true;
            break;
        }
        
        if (sporocilo.besedilo.indexOf(pravaImena[i]+ " se je preimenoval v ")==0)
        {
            pravaImena[i] = sporocilo.besedilo.substring(21+pravaImena[i].length, sporocilo.besedilo.length-1);
            jeNajden = true;
            break;
        }
    }
    
    if (!jeNajden)
    {
        for(var i in nalozenaImena)
        {
            if (sporocilo.besedilo.indexOf(nalozenaImena[i]+" (zasebno):")==0)
            {
                priv = true;
                novElement = divElementEnostavniTekst(sporocilo.besedilo);
            }
            
        }
    }
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
      nalozenaImena = uporabniki;
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
        var jeNajden = false;
        for(var j in pravaImena)
        {
            if (uporabniki[i]==pravaImena[j] && pravaImena[j] != trenutniVzdevek)
            {
                 $('#seznam-uporabnikov').append(divElementEnostavniTekst(navideznaImena[j] + " (" + pravaImena[j]+ ")"));
                 jeNajden = true;
                 break;
            }
        }
        if (!jeNajden)
            $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    
    // Klik na osebo sproži Krcanje
    // Kopija kode za pridruženje kanalu s klikom na ime kanala in kopija kode posiljanja zasebnega sprocila (oboje v Klepe_UI.js)
    $('#seznam-uporabnikov div').click(function() {
        var jeNajden = false;
        for(var i in pravaImena)
        {
            if ($(this).text() == navideznaImena[i]+" ("+pravaImena[i]+")")
            {
                sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno "' + pravaImena[i] + '" "&#9756;"');
                if (sistemskoSporocilo) 
                {
                    $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo.replace(pravaImena[i],navideznaImena[i]+ " (" + pravaImena[i]+")"))); // Doda poslano sporocilo v polje za sporocila posiljatelju
                    jeNajden = true;
                    break;
                }
            }
        }
        if (!jeNajden)
        {
            sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "&#9756;"');
            if (sistemskoSporocilo) 
            {
                $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo)); // Doda poslano sporocilo v polje za sporocila posiljatelju
            }
        }
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
